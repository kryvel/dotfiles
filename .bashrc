# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export TERM=xterm-256color

PS1='$( R_PS1=$?;
		if [[ $UID -eq 0 ]]; then
			echo -n "\e[01;31m${USER}\e[01;36m@\e[01;32m${HOSTNAME}:\e[00;37m${PWD}\e[0;0m"; 
			END_PS1="# ";
		else
			echo -n "\e[01;34m${USER}\e[01;36m@\e[01;32m${HOSTNAME}:\e[00;37m${PWD}\e[0;0m"; 
			END_PS1="$ ";
		fi	
		DATE_PS1=$(date +" [%d/%m/%y %H:%M:%S]"); 
		LEN_PS1=$(( $(tput cols) - ${#USER} - ${#HOSTNAME} - ${#PWD} - ${#DATE_PS1} - 2 )); 
		if [[ ${R_PS1} -eq 0 ]]; then 
			printf "\e[00;3m%${LEN_PS1}s" "${R_PS1}";
		else 
			printf "\e[01;31m%${LEN_PS1}s" "${R_PS1}";
		fi; 
		echo "${DATE_PS1}\e[m"; 
		echo -n "${END_PS1}"; 
	)'

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion


# don't put duplicate lines in the history.
HISTCONTROL=ignoredups:ignorespace
# append to the history file, don't overwrite it
shopt -s histappend     

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000 
HISTFILESIZE=20000 

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

#set PAGER
export PAGER=less

###### Aliases #####

# Modified commands ## {{{
alias diff='colordiff'              # requires colordiff package
alias grep='grep --color=auto'
alias df='df -h'
alias du='du -h'
alias mkdir='mkdir -p -v'
alias nano='nano -w'
alias dmesg='dmesg -HL'
# }}}

## New commands ## {{{
alias ..='cd ..'
alias cd..='cd ..'
# }}}

## ls ## {{{
alias ls='ls -hF --color=auto'
alias lr='ls -R'                    # recursive ls
alias ll='ls -l'
alias la='ll -a'
alias lx='ll -BX'                   # sort by extension
alias lz='ll -rS'                   # sort by size
alias lt='ll -rt'                   # sort by date
alias lm='la | more'
# }}}

## Safety features ## {{{
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -I'                    # 'rm -i' prompts for every file
alias ln='ln -i'
alias cls=' echo -ne "\033c"'       # clear screen for real (it does not work in Terminology)
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
# }}}
