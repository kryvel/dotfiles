# ` is escape sequence
unbind C-b
set-option -g prefix `
set-option -g prefix2 §

bind ` send-key `

# History size
set-option -g history-limit 50000

# Copy mode
unbind [
bind [ copy-mode

# switch panes with Tab
unbind Tab
bind Tab select-pane -t :.+

# Open man page with /
bind / command-prompt "split-window -h 'exec man %%'"

# resize pane with arrow keys
# -r: can be repeated without pressing prefix again (500ms after last '-r' action or prefix)
unbind Left
unbind Right
unbind Down
unbind Up
bind -r Left resize-pane -L 2
bind -r Right resize-pane -R 2
bind -r Down resize-pane -D 2
bind -r Up resize-pane -U 2

# New window
unbind ^C
bind c new-window

# reload config without killing server
bind R source-file ~/.tmux.conf \; display-message "  Config reloaded..."

# detach ^D d
unbind ^D
bind ^D detach

# displays * 
unbind *
bind * list-clients

# Lockscreen ^X x
unbind x
bind x lock-server

# Previous tab
unbind <
bind < previous-window

# Next tab
unbind >
bind > next-window

# Rename tab
unbind n
bind n command-prompt "rename-window %%"

# Tabs list
unbind w
bind w list-windows

# kill K k
unbind K
bind K confirm-before "kill-window"
unbind k
bind k confirm-before "kill-window"

# Split horizontal
unbind |
bind | split-window -h

# Split vertical
unbind _
bind _ split-window -v

# Start windows and panes at 1, not 0
set -g base-index 1
set -g pane-base-index 3

# panes
set -g pane-border-style fg=black
set -g pane-active-border-style fg=brightred

## Status bar design
# status line
set -g status-justify left
set -g status-style bg=default,fg=colour12
set -g status-interval 30

# messaging
set -g message-style fg=black,bg=yellow
set -g message-command-style fg=blue,bg=black


# Info on left
set -g status-left ''

# loud or quiet?
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

set -g default-terminal "xterm-256color"

# The modes {
setw -g clock-mode-colour colour135
setw -g mode-style fg=colour196,bg=colour238,bold
# }

# The panes {
set -g pane-border-style bg=colour235,fg=colour238
set -g pane-active-border-style bg=colour236,fg=colour51
# }

# The statusbar {
set -g status-position top
set -g status-style bg=colour234,fg=colour125,dim
set -g status-left ''
set -g status-right '#[fg=colour16,bg=colour241,bold] %d.%m #[fg=colour16,bg=colour245,bold] %H:%M '
set -g status-right-length 50
set -g status-left-length 20

setw -g window-status-current-style fg=colour81,bg=colour239,bold
setw -g window-status-current-format ' #I#[fg=colour15]:#[fg=colour255]#W#[fg=colour50]#F '

setw -g window-status-style fg=colour15,bg=colour236,none
setw -g window-status-format ' #I#[fg=colour15]:#[fg=colour250]#W '

setw -g window-status-bell-style fg=colour255,bg=colour1,bold
# }

# The messages {
set -g message-style fg=colour232,bg=colour166,bold
# }

