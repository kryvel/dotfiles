## Installation

### Required packages
	`sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
	`sudo apt install zsh`
	`chsh -s /bin/zsh`
	`git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k`
	`git clone --depth=1 https://github.com/vim-airline/vim-airline ${HOME}/.vim/pack/dist/start/vim-airline`
	
Remember to run `:helptags ~/.vim/pack/dist/start/vim-airline/doc`
